const express = require('express');
const models = require('../models');

const routes = express.Router();

module.exports = routes;

routes.get('/api/dispute-center/transactions', async (request, response, next) => {
    try {
        const transactions = await request.services.transactions('GET', '/api/transactions');
        const disputes = await models.disputes.listByTransactionIds(transactions.map(t => t.id));
        const users = await request.services.authentication('GET', `/api/authentication`);
        response.json({
            transactions: transactions
                .map(transaction => ({
                    transaction,
                    dispute: disputes.find(d => d.transaction_id === transaction.id) || {}
                }))
                .map(({ transaction, dispute }) => ({
                    ...transaction,
                    disputeStatus: dispute.status,
                    disputeReason: dispute.reason,
                })),
             users: users || []
        });
    }
    catch (error) {
        next(error);
    }
});

routes.get('/api/dispute-center/transactions/:id', async (request, response, next) => {
    try {
        const transaction = await request.services.transactions('GET', `/api/transactions/${request.params.id}`);
        const dispute = await models.disputes.getByTransactionId(transaction.id, { includeStatusLog: true }) || {};
        const users = await request.services.authentication('GET', `/api/authentication`);

        response.json({
            transaction: {
                ...transaction,
                disputeStatus: dispute.status,
                disputeReason: dispute.reason,
            },
            dispute_status_log: dispute.status_log || [],
            users: users || []
        });
    }
    catch (error) {
        next(error);
    }
});

//check dispute status, return if it can dispute
routes.get('/api/dispute-center/dispute/ifCanDispute/:id', async (request, response, next) => {
    try {
       const result = await models.disputes.ifCanDispute(request.params.id) || {};


        response.json({
            result 
        });
    }
    catch (error) {
        next(error);
    }
});

//submit dispute request,save dispute info
routes.post('/api/dispute-center/dispute/setDispute', async (request, response, next) => {
    try {
       const result = await models.disputes.setDispute(request.body) || {};

        response.json({
            result 
        });
    }
    catch (error) {
        next(error);
    }
});
//accept or reject request,deal it, return result
routes.post('/api/dispute-center/dispute/doApproval', async (request, response, next) => {
    try {
       const result = await models.disputes.doApproval(request.body) || {};

        response.json({
            result 
        });
    }
    catch (error) {
        next(error);
    }
});

//refresh token
routes.get('/api/dispute-center/dispute/refreshToken', async (request, response, next) => {
    try {
       const  result  = await request.services.authentication('GET', '/api/refreshToken');//authentication
        //const token=user.token;
        //sessionStorage.setItem('session_auth_token', token);
        response.json({  result     });
    } catch (error) {
        next(error);
    }
});
