const pg = require('pg');

module.exports = {
    getByTransactionId,
    listByTransactionIds,
    ifCanDispute,
    setDispute,
    doApproval
};

async function getByTransactionId(id, options) {
    const client = new pg.Client();
    await client.connect();
    try {
        const { rows: [dispute] } =
            await client.query(`
                SELECT *, (
                    SELECT status FROM dispute_status_log
                    WHERE dispute_id = dispute.id AND is_current = TRUE
                )
                FROM dispute
                WHERE transaction_id = $1 AND is_current = TRUE`,
                [id],
            );

        if (dispute && options && options.includeStatusLog) {
            const { rows: log } =
                await client.query(`
                    SELECT *
                    FROM dispute_status_log
                    WHERE dispute_id = $1`,
                    [dispute.id],
                );

            dispute.status_log = log;
        }

        return dispute;
    }
    finally {
        await client.end();
    }
}

async function listByTransactionIds(ids) {
    const client = new pg.Client();
    await client.connect();
    try {
        const { rows: disputes } =
            await client.query(`
                SELECT *, (
                    SELECT status FROM dispute_status_log
                    WHERE dispute_id = dispute.id AND is_current = TRUE
                )
                FROM dispute
                WHERE transaction_id = ANY($1) AND is_current = TRUE
                `,
                [ids],
            );

        return disputes;
    }
    finally {
        await client.end();
    }
}
//check dispute status, return if it can dispute
async function ifCanDispute(id) {
    const client = new pg.Client();
    await client.connect();
    try {
        const { rows: [dispute] } =
            await client.query(`
                SELECT *, (
                    SELECT status FROM dispute_status_log
                    WHERE dispute_id = dispute.id AND is_current = TRUE
                )
                FROM dispute
                WHERE transaction_id = $1 AND is_current = TRUE`,
                [id],
            );
        if (dispute==null ) return true;
        
        const { rows: log } =
            await client.query(`
                SELECT *
                FROM dispute_status_log
                WHERE dispute_id = $1 and  is_current = TRUE`,
                [dispute.id],
            );

        if(log.status=='Rejected') return true;
        
        return false;
    }
    finally {
        await client.end();
    }
}
//submit dispute request,save dispute info
async function setDispute(params){
    const client = new pg.Client();
    await client.connect();
    try {
        let result={code:1,msg:"dispute is submitted to admin successfully(already stored in db. The admin will review it later)"};
        //return params;
        if(params.reason==null || params.reason==undefined){
            result={code:-1,msg:"reason is null"};
            return result;
        }
        if(params.reason.length<20){
            result={code:-2,msg:"reason's length should be at least 20 character."};
            return result;
        }
        const { rows:res1} =
            await client.query(`
                insert into dispute(transaction_id,reason,is_current) values($1,$2,$3) RETURNING id`,
                [params.id,params.reason,false],
            );

        const disputeId =res1[0].id;

        let currTime = new Date();

        const { rows:res2} =
            await client.query(`
                insert into dispute_status_log(dispute_id,status,user_id,time,is_current) values($1,$2,$3,$4,$5)`,
                [disputeId,params.status,params.uid,currTime,false],
        );
        
        return result;
    } finally {
        await client.end();
    }
}

//accept or reject request,deal it, return result
async function doApproval(params){
    const client = new pg.Client();
    await client.connect();
    let result={code:1,msg:"submit sucessfully"};
    try {
        if(params.flag==null || params.flag===undefined){
            result={code:-1,msg:"param-flag is null"};
            return result;
        }
        const { rows: [dispute] } =
            await client.query(`
              select * from dispute where transaction_id=$1
            `,
                [params.id],
            );
        const disputeId =dispute.id;
        if(disputeId==null){
            result={code:-2,msg:"this dispute info doesnot exist"};
            return result;
        }
        // 1=accept,0=rejectdoApproval1
        // 1=accept,0=rejectdoApproval1,
        // 1=accept,0=rejectdoApproval1
        const v_status=params.flag==1?'Accepted':'Rejected';
        let currTime = new Date();
        const { rows:res2} =
            await client.query(`
            update dispute_status_log set is_current=false where dispute_id=$1 `,
                [disputeId],
            );
        const { rows:res3} =
            await client.query(`
            insert into dispute_status_log(dispute_id,status,user_id,time,is_current) values($1,$2,$3,$4,$5) `,
                [disputeId,v_status,params.uid,currTime,true],
            );
        return result;
    } finally {
        await client.end();
    }
}
